package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.SalaryNoMatchException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {


    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee getById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee getInsert(Employee employee) {
        if(employee.ageNotMatch()){
            throw new AgeErrorException();
        }
        if(employee.ageSalaryNotMatch()){
            throw new SalaryNoMatchException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee getUpdate(int id, Employee employee) {
        if(!employee.isStatus()){
            throw new EmployeeNotFoundException();
        }
        return employeeRepository.update(id, employee);
    }

    public Employee getDelete(int id) {
       return employeeRepository.delete(id);
    }
}
