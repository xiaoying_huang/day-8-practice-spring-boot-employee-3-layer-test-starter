package com.afs.restapi;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.SalaryNoMatchException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {
    EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    EmployeeService employeeService = new EmployeeService(employeeRepository);
    @Test
    void should_return_Age_Error_Exception_when_insert_employee_given_employee_age_15_and_66() {
        //given
        Employee employeeYoung = new Employee(1, "Zhangsan", 15, "Man", 1000);
        Employee employeeOld = new Employee(2, "Lisi", 66, "Female", 20000);

        //when then
        Assertions.assertThrows(AgeErrorException.class, ()->employeeService.getInsert(employeeYoung));
        Assertions.assertThrows(AgeErrorException.class, ()->employeeService.getInsert(employeeOld));

        verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    void should_return_rejection_when_insert_employee_given_employee_over_30_and_salary_below_20000() {
        //given
        EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
        EmployeeService employeeService = new EmployeeService(employeeRepository);

        Employee employeePool = new Employee(1, "Bob", 30, "Male", 2000);

        //when then
        Assertions.assertThrows(SalaryNoMatchException.class, ()->employeeService.getInsert(employeePool));
        verify(employeeRepository, Mockito.times(0)).insert(any());

    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule_black_box() {
        //given
        Employee employee = new Employee(1, "Bob", 30, "Male", 35000);

        Employee employeeToReturn = new Employee(1, "Bob", 30, "Male", 35000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        //when 添加员工，set的东西是true，验证返回的东西也是true，方法没有改变状态
        Employee employeeSaved = employeeService.getInsert(employee);

        //then
        assertTrue(employeeSaved.isStatus());
    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule_white_box() {
        //given
        Employee employee = new Employee(1, "Bob", 30, "Male", 35000);

        //when
        Employee employeeSaved = employeeService.getInsert(employee);

        //then //有没有真实的存进去，并且状态为true
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            assertTrue((employeeToSave.isStatus()));
            return true;
        }));

    }

    @Test
    void should_return_employee_with_status_false_when_delete_employee_given_a_employee() {
        //given
        Employee employeeToReturn = new Employee(1, "Zhangsan", 30, "Male", 10000);
        employeeToReturn.setStatus(false);
        when(employeeRepository.delete(anyInt())).thenReturn(employeeToReturn);

        //when
        Employee employeeDeleted = employeeService.getDelete(1);


        //then
        assertFalse(employeeDeleted.isStatus());//验证返回值
        //验证传参
        verify(employeeRepository).delete(Mockito.eq(1));
    }

    @Test
    void should_throw_EmployeeNotFoundException_when_update_given_a_employee_is_false_status() {
        //given
        Employee employee = new Employee(1, "Zhangsan", 30, "Male", 10000);
        employee.setStatus(false);

        Employee employeeUpdated = new Employee(1, "Zhangsan", 32, "Male", 12000);

        //when then
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.getUpdate(1, employeeUpdated));//验证返回值
    }


}