package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        clearAll();
    }
    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        //given
        Employee newEmployeeSusan = new Employee(1, "Susan", 22, "Female", 10000);
        employeeRepository.insert(newEmployeeSusan);

        Company newCompanySpring = new Company(1, "spring", employeeRepository.findAll());
        companyRepository.addCompany(newCompanySpring);

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());
        Company companyGet = companies.get(0);
        assertEquals(1, companyGet.getId());
        assertEquals("spring", companyGet.getCompanyName());
    }


    @Test
    void should_return_company_spring_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Employee newEmployeeSusan = new Employee(1, "Susan", 22, "Female", 10000);
        Employee newEmployeeBob = new Employee(2, "Bob", 24, "Male", 12000);
        List<Employee> employees1 = new ArrayList<>();
        List<Employee> employees2 = new ArrayList<>();
        employees1.add(newEmployeeSusan);
        employees2.add(newEmployeeBob);

        Company newCompanySpring = new Company(1, "spring", employees1);
        Company newCompanySummer = new Company(2, "summer", employees2);
        companyRepository.addCompany(newCompanySpring);
        companyRepository.addCompany(newCompanySummer);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("spring"));
    }
//    @Test
//    void should_return_employees_in_company_with_id_1_when_perform_get_by_id_given_companies__in_repo() throws Exception {
//        //given
//        Employee newEmployeeSusan = getNewEmployeeSusan();
//        Employee newEmployeeBob = getNewEmployeeBob();
//        List<Employee> employees = new ArrayList<>();
//        employees.add(newEmployeeSusan);
//        employees.add(newEmployeeBob);
//
//        Company newCompanySpring = new Company(1, "spring", employees);
//        companyRepository.addCompany(newCompanySpring);
//
//        //when
//        mockMvc.perform(MockMvcRequestBuilders.get("/1/employees"))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(jsonPath("$.id").isNumber())
//                .andExpect(jsonPath("$.id").value(1))
//                .andExpect(jsonPath("$.companyName").isString())
//                .andExpect(jsonPath("$.companyName").value("spring"))
//                .andExpect(jsonPath("$.employees"))
//
//    }



    public void clearAll() {
        companyRepository.getCompanies().clear();
    }

}