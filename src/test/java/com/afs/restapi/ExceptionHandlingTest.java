package com.afs.restapi;

import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.lang.annotation.ElementType;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ExceptionHandlingTest {
    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
    }
    @Test
    void should_throw_NotFoundException_when_perform_get_given_employees_in_repo_and_a_wrong_id() throws Exception {
        //given
        Employee employeeSusan = new Employee(1, "Susan", 30, "Female", 20000);
        employeeRepository.insert(employeeSusan);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/{id}", 2))
                .andExpect(status().isNotFound());
    }

//    @Test
//    void should_throw_NotFoundException_when_perform_post_given_a_employee_false_status() throws Exception {
//        //given
//        Employee employeeSusan = new Employee(1, "Susan", 30, "Female", 20000);
//        employeeSusan.setStatus(false);
//        employeeRepository.insert(employeeSusan);
//        Employee employeeUpdated = employeeRepository.update(1, employeeSusan);
//
//        //when then
//        mockMvc.perform(MockMvcRequestBuilders.get("/employees/{id}", 1))
//                .andExpect(status().isNotFound());
//    }
}
