## Objective 事实
- Learned about the architecture of Spring Boot, 
- Understood Test-Driven Development (TDD) principles in Spring Boot, 
- Learned how to handle errors in API development using Spring Boot.


## Reflective 反应

- It's hard for me to understand the meaning of API Error Handling.
- It's crazy and difficult for me to complete the program homework by using the thing today I learned.


## Interpretive 解释/分析
1. Classes calling each other, logic cannot be clarified.
2. Not familiar with SprintBoot, unable to use various tags and methods and do not know the meaning of them.


## Decisional 决定/结果
1. Use your free time to review relevant materials and systematically learn SpringBoot.
2. More practise.
3. Ask others for help.
4. Organize notes timely every day